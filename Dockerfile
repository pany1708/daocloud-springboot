FROM openjdk:8-jre-alpine
LABEL maintainer "yong.pan@daocloud.io"
WORKDIR root/myapp/
ADD target/spring-boot-web-0.0.1-SNAPSHOT.jar spring-boot-web.jar
RUN  ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime  \
    && echo "Asia/Shanghai" > /etc/timezone
ENTRYPOINT java -jar \
 -XX:+PrintFlagsFinal \
 -XX:+UnlockExperimentalVMOptions \
 -XX:+UseCGroupMemoryLimitForHeap \
$JAVA_OPTS spring-boot-web.jar